import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#fff",
      alignItems: "center",
      justifyContent: "center",
    },
   
    logo: {
        fontSize: 25,
        marginBottom: 40,
    },
   
    inputView: {
      width: "70%",
      height: 45,
      marginBottom: 20,
   
    //   alignItems: "center",
    },
   
    TextInput: {
      height: 50,
      flex: 1,
      padding: 10,
      marginLeft: 20,
      textAlign: 'center',
    },
   
    forgot_button: {
      height: 30,
      marginBottom: 10,
    },
   
    loginBtn: {
      width: "80%",
      borderRadius: 25,
      height: 50,
      // alignItems: "center",
      justifyContent: "center",
      marginTop: 20,
      backgroundColor: "#192537",
    },

    loginText: {
        color: "#fff"
    },

    errorMsg: {
      color:"#FD5757",
      marginBottom: 10,
    },

  });