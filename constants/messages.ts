export const Messages = {
    FIRST_NAME: "Please enter your First Name!",
    LAST_NAME: "Please enter your Last Name!",
    MOBILE: "Mobile Number is not Valid!",
    EMAIL: "Email is not valid!",
    PASSWORD: "Password must be 8 characters long!",
    TOO_SHORT: " Values is Too short!",
    TOO_LONG: "Value is Too Long!",
    FIELD_REQUIRED: "Field is required!",

}