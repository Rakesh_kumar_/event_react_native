import { StatusBar } from "expo-status-bar";
import { useState } from "react";
import { 
    Text, 
    TouchableOpacity, 
    View
 } from "react-native";
import { styles } from '../constants/css'
import { Formik } from "formik";
import { 
    Button, 
    Icon, 
    TextInput 
} from "@react-native-material/core";
import { activeInputColor } from "../constants/Colors";
import * as Yup from 'yup';
import { Messages } from "../constants/messages";


const LoginScreen = ({ navigation }) => {
    const [loading, setLoading] = useState(false)

    return(
      <>
        <View style={styles.container}>
          <Text style={styles.logo}>Log In</Text>
              <StatusBar style="auto"/>
                <Formik
                    initialValues={{
                        email: '',
                        password: '',
                    }}
                    validationSchema={Yup.object().shape({
                        email: Yup.string().email(Messages.EMAIL).required(Messages.FIELD_REQUIRED),
                        password: Yup.string().min(8, Messages.TOO_SHORT).max(50, Messages.TOO_LONG).required(Messages.FIELD_REQUIRED),
                    })}
                    onSubmit={values => {
                        // same shape as initial values
                        console.log("dsadasda", values);
                    }}
                    >
                    {({ errors, touched, handleChange, handleBlur, handleSubmit, values }) => (
                        <>
                        <View style={styles.inputView}>
                            <TextInput
                                variant="standard"
                                label="Email*"
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                                value={values.email}
                                trailing={errors.email && touched.email ? <Icon name="exclamation-thick" color="red" size={20}/> : null}
                                color={activeInputColor}
                                />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput
                                variant="standard"
                                label="Password*"
                                secureTextEntry={true}
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                value={values.password}
                                trailing={errors.password && touched.password ? <Icon name="exclamation-thick" color="red" size={20}/> : null}
                                color={activeInputColor}
                                />
                        </View>

                        <TouchableOpacity>
                            <Text style={styles.forgot_button}>Forgot Password?</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={styles.forgot_button} onPress={() => navigation.navigate('Signup')}> Don't have account? Sign Up</Text>
                        </TouchableOpacity>
                
                        <Button
                            style={styles.loginBtn} 
                            onPress={() => handleSubmit(values)} 
                            title="LOGIN"
                            loading={loading}
                        />
                        </>
                        )}
                </Formik>
        </View> 
        </>
    );
}

export default LoginScreen;