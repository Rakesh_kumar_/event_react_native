import { StatusBar } from "expo-status-bar";
import { 
    Text, 
    TouchableOpacity, 
    View
 } from "react-native";
import { styles } from "../constants/css";
import { Messages } from "../constants/messages";
import { Formik } from 'formik';
import * as Yup from 'yup';
import { 
    TextInput,
    Stack,
    Icon,
    Button,
 } from "@react-native-material/core";
import { useState } from "react";
import { activeInputColor } from "../constants/Colors";

const SignupScreen = ({navigation}) => {
    const [loading, setLoading] = useState(false);

    return(
        <View style={styles.container}>
        <Text style={styles.logo}>Create an Account</Text>
            <StatusBar style="auto"/>
            <Formik
                initialValues={{
                    first_name: '',
                    last_name: '',
                    email: '',
                    password: '',
                }}
                validationSchema={Yup.object().shape({
                    first_name: Yup.string().min(3, Messages.TOO_SHORT).max(50, Messages.TOO_LONG).required(Messages.FIELD_REQUIRED),
                    last_name: Yup.string().min(3, Messages.TOO_SHORT).max(50, Messages.TOO_LONG).required(Messages.FIELD_REQUIRED),
                    email: Yup.string().email(Messages.EMAIL).required(Messages.FIELD_REQUIRED),
                    password: Yup.string().min(8, Messages.TOO_SHORT).max(50, Messages.TOO_LONG).required(Messages.FIELD_REQUIRED),
                })}
                onSubmit={values => {
                    // same shape as initial values
                    console.log("dsadasda", values);
                }}
                >
                {({ errors, touched, handleChange, handleBlur, handleSubmit, values }) => (
                    <>
                    <View style={styles.inputView}>
                        <TextInput
                            variant="standard"
                            label="First Name*"
                            onChangeText={handleChange('first_name')}
                            onBlur={handleBlur('first_name')}
                            value={values.first_name}
                            trailing={errors.first_name && touched.first_name ? <Icon name="exclamation-thick" color="red" size={20}/> : null}
                            color={activeInputColor}
                            />
                    </View>
                    <View style={styles.inputView}>
                        <TextInput
                            variant="standard"
                            label="Last Name*"
                            onChangeText={handleChange('last_name')}
                            onBlur={handleBlur('last_name')}
                            value={values.last_name}
                            trailing={errors.last_name && touched.last_name ? <Icon name="exclamation-thick" color="red" size={20}/> : null}
                            color={activeInputColor}
                            />
                    </View>
                    <View style={styles.inputView}>
                        <TextInput
                            variant="standard"
                            label="Email*"
                            onChangeText={handleChange('email')}
                            onBlur={handleBlur('email')}
                            value={values.email}
                            trailing={errors.email && touched.email ? <Icon name="exclamation-thick" color="red" size={20}/> : null}
                            color={activeInputColor}
                            />
                    </View>
                    <View style={styles.inputView}>
                        <TextInput
                            variant="standard"
                            label="Password*"
                            secureTextEntry={true}
                            onChangeText={handleChange('password')}
                            onBlur={handleBlur('password')}
                            value={values.password}
                            trailing={errors.password && touched.password ? <Icon name="exclamation-thick" color="red" size={20}/> : null}
                            color={activeInputColor}
                            />
                    </View>

            <TouchableOpacity>
                <Text style={styles.forgot_button} onPress={() => navigation.navigate('Root')}>Already have account? Log In</Text>
            </TouchableOpacity>
           
            <Button 
                style={styles.loginBtn} 
                onPress={() => handleSubmit(values)} 
                title="Register"
                loading={loading}
            />
            </>
            )}
            </Formik>
        </View>
    );
}


export default SignupScreen;

