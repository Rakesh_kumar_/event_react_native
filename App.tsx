import { IconComponentProvider } from '@react-native-material/core';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { StatusBar } from 'expo-status-bar';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        <IconComponentProvider IconComponent={MaterialCommunityIcons}>
        <Navigation colorScheme={colorScheme} />
        </IconComponentProvider>
        <StatusBar />
      </SafeAreaProvider>
    );
  }
}
